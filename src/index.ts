#! /usr/bin/env node
import populateApplicants from "./populateApplicants";
import getAcceptedApplicants from "./getAcceptedApplicants";
import getProperties from "./getProperties";

const args = process.argv.slice(2);

switch (args[0]) {
    case "get-accepted": {
        getAcceptedApplicants();
        break;
    }
    case "populate-datastore": {
        populateApplicants(args[1]);
        break;
    }
    case "get-property": {
        getProperties(args[2], args[3]);
        break;
    }
    default: {
        console.log(`Usage: nhs-app-utils [command] [options]
        
Where [command] is one of:
        - get-accepted: Get all Applicants who scored a 6 or above on the points system. Outputs to result.json in CWD due to technical constraints.
        - populate-datastore [csv_file_path]: Takes a CSV list of applicants as input and puts them in the Google Cloud Datastore as Applicants.
        - get-property -i [file.json] [property_name]
        `);
    }
}