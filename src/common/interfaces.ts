export interface ILeadershipEntry {
  yearsInvolved: "9th" | "10th" | "11th";
  description: string;
}

export interface IEntityKey {
  namespace?: any;
  id: number;
  kind: "Applicant";
  path?: any;
}

export interface IApplicant {
  points?: number[] | string;
  first_name: string;
  last_name: string;
  ID?: number;
  email?: string;
}
