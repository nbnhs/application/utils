const { Datastore } = require("@google-cloud/datastore");
export const datastore = new Datastore({
  projectId: process.env.PROJECTID
});

export default async function getApplicants() {
  const [applicants] = await datastore.runQuery(
    datastore.createQuery("Applicant")
  );
  return applicants;
}
