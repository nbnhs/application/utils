import getApplicants from "./common/datastore";
import { IApplicant } from "./common/interfaces";
import * as fs from "fs";

async function createApplicantArray(): Promise<IApplicant[]> {
  const raw = await getApplicants();
  const res: IApplicant[] = [];
  for (const rawApp of raw) {
    let pts: number[];
    try {
      pts = (rawApp["points"] as string).split(",").map(Number);
    } catch {
      pts = [0, 0, 0, 0];
    }

    const app: IApplicant = {
      first_name: rawApp["first_name"],
      last_name: rawApp["last_name"],
      points: pts
    };

    res.push(app);
  }
  return res;
}

export default function getAcceptedApplicants(): void {
  createApplicantArray().then((val: IApplicant[]) => {
    const res: IApplicant[] = [];
    for (const app of val) {
      if (
        (app.points as number[]).reduce((a, b): number => {
          return a + b;
        }, 0) >= 6
      ) {
        res.push(app);
      }
    }

    fs.writeFile("./result.json", JSON.stringify(res), err => {
      console.log("Results have been written to ./result.json");
      if (err) {
        console.error(err);
      }
    });
  });
}
