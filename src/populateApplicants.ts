import { IApplicant } from "./common/interfaces";
import { parse } from "papaparse";
import * as fs from "fs";
import { datastore } from "./common/datastore";

function toString(a: IApplicant): string {
  return `Applicant ${a.first_name} ${a.last_name} (${a.email}): ${a.ID}`;
}

function interpretCSV(s: string): Promise<string[][]> {
  return new Promise((resolve, reject) => {
    parse(s, {
      complete: results => {
        // console.log(results.data);
        resolve(results.data);
      },
      error: error => {
        reject({
          status: error,
          statusText: error.message
        });
      }
    });
  });
}

async function convertToApplicants(
  arrayPromise: Promise<string[][]>
): Promise<IApplicant[]> {
  const arr = await arrayPromise;

  const init = arr[0];
  const id = init.findIndex(e => e === "ID Number");
  const firstName = init.findIndex(e => e === "First Name");
  const lastName = init.findIndex(e => e === "Last Name");
  const email = init.findIndex(e => e === "Email");
  delete arr[0];

  const result: IApplicant[] = [];
  arr.forEach((element: string[]) => {
    result.push({
      first_name: element[firstName],
      last_name: element[lastName],
      ID: parseInt(element[id]),
      email: element[email]
    });
  });

  return result;
}

async function csv2Database(csv: string): Promise<string[]> {
  const res: string[] = [];

  const promise = interpretCSV(csv);
  const data: IApplicant[] = await convertToApplicants(promise);

  return new Promise((resolve, _) => {
    data.forEach(applicant => {
      const obj = {
        key: datastore.key(["Applicant", applicant.ID]),
        data: {
          first_name: applicant.first_name,
          last_name: applicant.last_name,
          signature: "Incomplete",
          leadership: "Incomplete",
          service: "Incomplete",
          teacher: "Incomplete",
          points: "0,0,0,0",
        }
      };

      datastore
        .save(obj)
        .then((_: any) => {
          res.push(String(applicant.email));
          console.log(toString(applicant));
        })
        .catch((err: any) => {
          console.log(err);
        });
    });

    resolve(res);
  });
}

export default function populateApplicants(input: string): void {
    csv2Database(String(fs.readFileSync(input))).then((val: string[]) => {
        console.log(val);
    });
}
