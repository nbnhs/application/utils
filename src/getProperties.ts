import * as fs from "fs";
import { IApplicant } from "./common/interfaces";

export default function getProperties(input_file: string, property: string): void {
    const data: IApplicant[] = JSON.parse(String(fs.readFileSync(input_file)));

    for (const app of data) {
        if (property === "full_name") {
            console.log(`${app.first_name} ${app.last_name}`);
        } else {
            console.log(app[property]);
        }
    }
}