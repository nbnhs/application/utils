"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
function getProperties(input_file, property) {
    const data = JSON.parse(String(fs.readFileSync(input_file)));
    for (const app of data) {
        if (property === "full_name") {
            console.log(`${app.first_name} ${app.last_name}`);
        }
        else {
            console.log(app[property]);
        }
    }
}
exports.default = getProperties;
