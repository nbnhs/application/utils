#! /usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const populateApplicants_1 = __importDefault(require("./populateApplicants"));
const getAcceptedApplicants_1 = __importDefault(require("./getAcceptedApplicants"));
const getProperties_1 = __importDefault(require("./getProperties"));
const args = process.argv.slice(2);
switch (args[0]) {
    case "get-accepted": {
        getAcceptedApplicants_1.default();
        break;
    }
    case "populate-datastore": {
        populateApplicants_1.default(args[1]);
        break;
    }
    case "get-property": {
        getProperties_1.default(args[2], args[3]);
        break;
    }
    default: {
        console.log(`Usage: nhs-app-utils [command] [options]
        
Where [command] is one of:
        - get-accepted: Get all Applicants who scored a 6 or above on the points system. Outputs to result.json in CWD due to technical constraints.
        - populate-datastore [csv_file_path]: Takes a CSV list of applicants as input and puts them in the Google Cloud Datastore as Applicants.
        - get-property -i [file.json] [property_name]
        `);
    }
}
