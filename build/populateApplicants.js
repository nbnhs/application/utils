"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const papaparse_1 = require("papaparse");
const fs = __importStar(require("fs"));
const datastore_1 = require("./common/datastore");
function toString(a) {
    return `Applicant ${a.first_name} ${a.last_name} (${a.email}): ${a.ID}`;
}
function interpretCSV(s) {
    return new Promise((resolve, reject) => {
        papaparse_1.parse(s, {
            complete: results => {
                resolve(results.data);
            },
            error: error => {
                reject({
                    status: error,
                    statusText: error.message
                });
            }
        });
    });
}
function convertToApplicants(arrayPromise) {
    return __awaiter(this, void 0, void 0, function* () {
        const arr = yield arrayPromise;
        const init = arr[0];
        const id = init.findIndex(e => e === "ID Number");
        const firstName = init.findIndex(e => e === "First Name");
        const lastName = init.findIndex(e => e === "Last Name");
        const email = init.findIndex(e => e === "Email");
        delete arr[0];
        const result = [];
        arr.forEach((element) => {
            result.push({
                first_name: element[firstName],
                last_name: element[lastName],
                ID: parseInt(element[id]),
                email: element[email]
            });
        });
        return result;
    });
}
function csv2Database(csv) {
    return __awaiter(this, void 0, void 0, function* () {
        const res = [];
        const promise = interpretCSV(csv);
        const data = yield convertToApplicants(promise);
        return new Promise((resolve, _) => {
            data.forEach(applicant => {
                const obj = {
                    key: datastore_1.datastore.key(["Applicant", applicant.ID]),
                    data: {
                        first_name: applicant.first_name,
                        last_name: applicant.last_name,
                        signature: "Incomplete",
                        leadership: "Incomplete",
                        service: "Incomplete",
                        teacher: "Incomplete",
                        points: "0,0,0,0",
                    }
                };
                datastore_1.datastore
                    .save(obj)
                    .then((_) => {
                    res.push(String(applicant.email));
                    console.log(toString(applicant));
                })
                    .catch((err) => {
                    console.log(err);
                });
            });
            resolve(res);
        });
    });
}
function populateApplicants(input) {
    csv2Database(String(fs.readFileSync(input))).then((val) => {
        console.log(val);
    });
}
exports.default = populateApplicants;
