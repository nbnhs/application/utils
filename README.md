# NHS Application Utilities

Command-line application containing common-use utilities for the NHS Applications process. For Admins only.

## Build

```
make
npm i -g

nhs-app-utils --help
```
